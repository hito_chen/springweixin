package com.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.dao.NewsItemDao;
import com.entity.Newsitem;
import com.entity.Newstype;
@Repository("newsItemDao")
public class NewsItemDaoImpl extends BaseDaoImpl<Newsitem> implements NewsItemDao{
    @Override
    public void UpdateNewsType(Newstype oldnewstype, Newstype newnewsstype) {
	// TODO Auto-generated method stub
	Query query=this.getQuery("update Newsitem as n set n.newstype.id=? where n.newstype.id=?",newnewsstype.getId(),oldnewstype.getId());
	System.out.println(oldnewstype.getId());
	System.out.println(newnewsstype.getId());
	System.out.println(query.executeUpdate());
    }
}
