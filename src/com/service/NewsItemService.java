package com.service;

import java.util.List;

import com.dto.NewsItemDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.User;
/**
 * 新闻条目服务接口
 * NewsItemService.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月14日 下午1:38:38 
 * @version
 * @user micrxdd
 */
public interface NewsItemService {
    /**
     * 根据id和分页信息返回新闻条目列表
     * @param pageinfo
     * @param typeid
     * @return
     */
    public Pagers NewsLitemList(Pageinfo pageinfo,Integer typeid);
    /**
     * 保存新闻条目
     * @param dtos
     * @param user
     */
    public void SaveNewsitem(List<NewsItemDto> dtos,User user);
    /**
     * 更新新闻条目
     * @param dtos
     * @param user
     */
    public void UpdateNewsitem(List<NewsItemDto> dtos,User user);
    /**
     * 通过String格式的(ids=1,2,3)来进行删除操作
     * @param ids
     */
    public void DelNewsitemStringIds(String ids);
    /**
     * 通过数组来进行删除操作(ids=1&ids=2&ids=3)
     * @param ids
     */
    public void DelNewsitemIds(Integer[] ids);
}
