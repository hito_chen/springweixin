package com.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.MenuDao;
import com.dao.UserGroupDao;
import com.dto.MenuDto;
import com.entity.Group;
import com.entity.Menu;
import com.entity.User;
import com.service.MenusService;
import com.vo.BaseMenuVo;
import com.vo.MenuVo;
@Service("menusService")
public class MenusServiceImpl implements MenusService{
    @Resource
    private MenuDao menuDao;
    @Resource
    private UserGroupDao userGroupDao;
    @Override
    public List<MenuVo> Menus(User user) {
	// TODO Auto-generated method stub
	List<Menu> list = menuDao.findMenusByStringIds(user.getGroup().getMenusids());
	return f(list);
    }
    @Override
    public List<MenuVo> AllMenus() {
	// TODO Auto-generated method stub
	List<Menu> menus= menuDao.findAll();
	return f(menus);
    }
    public List<MenuVo> digui(Set<Menu> menu){
	List<MenuVo> menuVos = new ArrayList<MenuVo>();
	for (Menu m : menu) {
	    MenuVo menuVo = new MenuVo();
	    BeanUtils.copyProperties(m, menuVo);
	    menuVos.add(menuVo);
	    menuVo.setChildren(digui(m.getMenus()));
	}
	return menuVos;
    }
    
    public List<MenuVo> f(List<Menu> list){
	Map<Integer, MenuVo> map;
	List<MenuVo> menuVos=null;
	if (list.size() > 0) {
	    map = new HashMap<Integer, MenuVo>();
	    menuVos = new ArrayList<MenuVo>();
	    
	    for (Menu menu : list) {
		if(menu.getAction())continue;
		MenuVo vo = new MenuVo();
		BeanUtils.copyProperties(menu, vo,"menu");
		List<MenuVo> child = new ArrayList<MenuVo>();
		vo.setChildren(child);
		map.put(menu.getId(), vo);
	    }
	    for (Menu menu : list) {
		Menu pMenu=menu.getMenu();
		if(pMenu==null){
		    //System.out.println("根节点:"+menu.getText());
		    menuVos.add(map.get(menu.getId()));
		}else{
		    //System.out.println("设置叶子节点:"+menu.getText()+">"+pMenu.getText());
		    map.get(pMenu.getId()).getChildren().add(map.get(menu.getId()));
		}
	    }
	    return menuVos;
	}
	return null;
    }
    @Override
    public void SaveMenus(List<MenuDto> dtos) {
	// TODO Auto-generated method stub
	for (MenuDto menudTo : dtos) {
	    Menu menu=new Menu();
	    BeanUtils.copyProperties(menudTo, menu);
	    if(menudTo.get_parentId()!=null){
		Menu pmenu=new Menu();
		pmenu.setId(menudTo.get_parentId());
		menu.setMenu(pmenu);
	    }
	    menuDao.save(menu);
	}
    }
    @Override
    public void UpdateMenus(List<MenuDto> dtos){
	for (MenuDto menuDto : dtos) {
	    if(menuDto.getId()<0){
		Menu menu=new Menu();
		BeanUtils.copyProperties(menuDto, menu,"id","menu","menus");
		if(menuDto.get_parentId()!=null){
			Menu pmenu=new Menu();
			pmenu.setId(menuDto.get_parentId());
			menu.setMenu(pmenu);
		    }
		menuDao.save(menu);
	    }else{
		Menu menu=menuDao.findById(menuDto.getId());
		BeanUtils.copyProperties(menuDto, menu,"id","menu","menus");
	    }
	    
	}
    }
    public void DeleteMenus(Integer[] ids){
	menuDao.DelByIntids(ids);
    }
    @Override
    public List<MenuDto> AllMenusBase() {
	// TODO Auto-generated method stub
	return null;
    }
    @Override
    public List<BaseMenuVo> ListMenusByGroupId(Integer groupid) {
	// TODO Auto-generated method stub
	Group group=userGroupDao.findById(groupid);
	if(group==null){
	    return null;
	}
	String ids=group.getMenusids();
	ids=ids==null?"":ids;
	String[] id=ids.split(",");
	Map<String, Boolean> groupmap=new HashMap<String, Boolean>();
	for (String string : id) {
	    groupmap.put(string, false);
	}
	List<Menu> menus= menuDao.findAll();
	List<BaseMenuVo> menuVos =null;
	Map<Integer, BaseMenuVo> map;
	if (menus.size() > 0) {
	    map = new HashMap<Integer, BaseMenuVo>();
	    
	    
	    menuVos = new ArrayList<BaseMenuVo>();
	    for (Menu menu : menus) {
		BaseMenuVo vo = new BaseMenuVo();
		BeanUtils.copyProperties(menu, vo,"menu","checked");
		List<BaseMenuVo> child = new ArrayList<BaseMenuVo>();
		vo.setChildren(child);
		if(groupmap.get(menu.getId().toString())!=null){
		    System.out.println("+++++++++++++++++++=");
		    System.out.println(menu.getId());
		    vo.setChecked(true);
		}
		map.put(menu.getId(), vo);
	    }
	    for (Menu menu : menus) {
		Menu pMenu=menu.getMenu();
		if(pMenu==null){
		    System.out.println("根节点:"+menu.getText());
		    BaseMenuVo vo=map.get(menu.getId());
		    vo.setChecked(false);
		    menuVos.add(vo);
		}else{
		    System.out.println("设置叶子节点:"+menu.getText()+">"+pMenu.getText());
		    BaseMenuVo vo=map.get(pMenu.getId());
		    vo.setChecked(false);
		    vo.getChildren().add(map.get(menu.getId()));
		}
	    }
	}
	return menuVos;
    }
    @Override
    public List<Menu> menusBase(User user) {
	// TODO Auto-generated method stub
	return menuDao.findMenusByStringIds(user.getGroup().getMenusids());
    }

}
