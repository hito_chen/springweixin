package com.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.dao.NewsItemDao;
import com.dao.NewsTypeDao;
import com.dto.NewsItemDto;
import com.dto.Pageinfo;
import com.dto.Pagers;
import com.entity.Newsitem;
import com.entity.Newstype;
import com.entity.User;
import com.service.NewsItemService;
/**
 * 新闻条目服务
 * NewsItemServiceImpl.java
 * @author  microxdd
 * @time 创建时间：2014 2014年9月13日 下午2:25:12 
 * @version
 * @user micrxdd
 */
@Service("newsItemService")
public class NewsItemServiceImpl implements NewsItemService{
    @Resource
    private NewsItemDao newsItemDao;
    @Resource
    private NewsTypeDao newsTypeDao;

    @Override
    public Pagers NewsLitemList(Pageinfo pageinfo,Integer typeid) {
	// TODO Auto-generated method stub
	Pagers pagers;
	if(typeid==null){
	    pagers=newsItemDao.getForPage(pageinfo);
	}else{
	    Newstype newstype=new Newstype();
	    newstype.setId(typeid);
	    pagers=newsItemDao.getForPage(pageinfo, Restrictions.eq("newstype", newstype));
	}
	List<Newsitem> list=(List<Newsitem>) pagers.getRows();
	List<NewsItemDto> dtos=new ArrayList<NewsItemDto>();
	for (Newsitem newsitem : list) {
	    NewsItemDto dto=new NewsItemDto();
	    BeanUtils.copyProperties(newsitem, dto);
	    dto.setUsername(newsitem.getUser().getUsername());
	    dto.setTypeid(newsitem.getNewstype().getId());
	    dtos.add(dto);
	}
	pagers.setRows(dtos);
	return pagers;
    }

    @Override
    public void SaveNewsitem(List<NewsItemDto> dtos,User user) {
	// TODO Auto-generated method stub
	Timestamp timestamp=new Timestamp(System.currentTimeMillis());
	for (NewsItemDto newsItemDto : dtos) {
	    Newsitem newsitem=new Newsitem();
	    newsitem.setCtime(timestamp);
	    newsitem.setDescription(newsItemDto.getDescription());
	    Newstype newstype=newsTypeDao.findById(newsItemDto.getTypeid());
	    newsitem.setNewstype(newstype);
	    newsitem.setUser(user);
	    newsItemDao.save(newsitem);
	}
    }

    @Override
    public void UpdateNewsitem(List<NewsItemDto> dtos,User user) {
	// TODO Auto-generated method stub
	Timestamp timestamp=new Timestamp(System.currentTimeMillis());
	for (NewsItemDto newsItemDto : dtos) {
	    if(newsItemDto.getId()<0){
		    Newsitem newsitem=new Newsitem();
		    newsitem.setCtime(timestamp);
		    newsitem.setDescription(newsItemDto.getDescription());
		    Newstype newstype=newsTypeDao.findById(newsItemDto.getTypeid());
		    newsitem.setNewstype(newstype);
		    newsitem.setUser(user);
		    newsItemDao.save(newsitem);
	    }else{
		Newsitem newsitem =newsItemDao.findById(newsItemDto.getId());
		newsitem.setCtime(timestamp);
		newsitem.setDescription(newsItemDto.getDescription());
		newsitem.setUser(user);
	    }
	}
    }

    @Override
    public void DelNewsitemStringIds(String ids) {
	// TODO Auto-generated method stub
	newsItemDao.DelByStringids(ids);
    }

    @Override
    public void DelNewsitemIds(Integer[] ids) {
	// TODO Auto-generated method stub
	//newsItemDao.DelByIntids(ids);
	for (Integer id : ids) {
	    Newsitem newsitem=newsItemDao.findById(id);
	    newsItemDao.del(newsitem);
	}
    }
    
}
