package com.vo;
/**
 * NewsVo.java
 * @author  microxdd
 * @version 创建时间：2014 2014年9月13日 下午9:57:13 
 * micrxdd
 * 
 */
public class NewsVo {
    private Integer id;
    private String title;
    private String picUrl;
    private String url;
    private Integer itemid;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getPicUrl() {
        return picUrl;
    }
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public Integer getItemid() {
        return itemid;
    }
    public void setItemid(Integer itemid) {
        this.itemid = itemid;
    }
    
}
