package com.weixin.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
/**
 * 微信配置  已经废弃。
 * 
 * @see com.weixin.service.WeiXinServiceImpl
 * @author  microxdd
 * @time 创建时间：2014 2014年9月15日 下午8:53:18 
 * @version
 * @user micrxdd
 */
@Deprecated
public class Config {
    // 赋权类型
    //public static String grant_type = "client_credential";

    // 修改为开发者申请的appid
   // public static String APPID = "wxb77ebe16b2abdd83";

    // 修改为开发者申请的secret密钥
    //public static String SECRET = "ca3ef339ee50485541b63151f0b8848a";
    private static Properties props = new Properties();

    static {
	try {
	    // play框架下要用这种方式加载
	    // props.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("/wechat.properties"));
	    props.load(Config.class.getResourceAsStream("/wei.properties"));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public static String get(String key) {
	return props.getProperty(key);
    }

    public static void setProps(Properties p) {
	props = p;
    }
}
