package com.weixin.msg;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.weixin.vo.Article;
@XmlRootElement(name="xml")
public class ArticleAndPicsMsg extends Msg{
	private List<Article> articles;
	public ArticleAndPicsMsg() {
		// TODO Auto-generated constructor stub
		this.msgType=MsgEmum.news.toString();
		articles=new ArrayList<Article>();
	}
	@XmlElement(name="ArticleCount")
	public int getArticleCount() {
		return articles.size();
	}
	@XmlElementWrapper(name="Articles")
	@XmlElement(name="item")
	public List<Article> getArticles() {
		return articles;
	}
	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
	public void AddArticles(Article article){
		this.articles.add(article);
	}
	
}
